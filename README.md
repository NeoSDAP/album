# Magisk Module: Xperiance Album

This module brings back the Album App to Xperia Devices.

## Build

You can build the module on your own by running `./gradlew packageMagiskModule`. The module zip file can be found in `build/outputs/magisk`.
